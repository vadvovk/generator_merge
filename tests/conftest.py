import pytest
import time

@pytest.fixture(autouse=True)
def setup_and_tear_down():
    """Setup and restore environment before and after test"""
    print('\n\n' + '_' * 30 + 'Test Case Started' + '_' * 30)
    # TBD
    yield
    print('_' * 30 + 'Test Case Finished' + '_' * 30)
    # TBD

@pytest.fixture(autouse=True)
def test_duration():
    """Inform test duration - useful for big data"""
    start = time.time()
    yield
    stop = time.time()
    delta = stop - start
    print('Test Duration : {:0.3} seconds'.format(delta))
