from src.generator_merge import merge
import pytest

@pytest.mark.parametrize('input_iterable, current_iterable, expected_result',
                         [([1, 3, 4], [1, 3, 4], 6),
                          ([2, 3, 8], [2, 10], 5),
                          ([1, 3], [2], 3),
                          ([], [10], 1),
                          ([], [], 0),
                          ])
def test_iterable_len_after_merging(input_iterable, current_iterable, expected_result):
    print('\nTest Description: Check that length merged iterable the same as '
          'length input + current iterables')
    result = len(merge(input_iterable, current_iterable))
    print('The data for evidence:')
    print('-> Length input iterable: ' + str(len(input_iterable)))
    print('-> Length current iterable: ' + str(len(current_iterable)))
    print('Actual Result: Length merged iterables is ' + str(result))
    print('Expected Result: Length merged iterables should be ' + str(expected_result))
    assert result == expected_result

@pytest.mark.parametrize('input_iterable, current_iterable, expected_result',
                         [([1, 3, 4], [1, 3, 4], [1, 1, 3, 3, 4, 4]),
                          ([2, 3, 8], [2, 10], [2, 2, 3, 8, 10]),
                          ([1, 3], [2], [1, 2, 3]),
                          ([], [10], [10]),
                          ([10, 20, 30], [], [10, 20, 30]),
                          ])
def test_iterable_content_after_merging(input_iterable, current_iterable, expected_result):
    print('\nTest Description: Check that content merged iterable the same as '
          'content input iterable + content current iterable')
    result = merge(input_iterable, current_iterable)
    print('The data for evidence:')
    print('-> Content input iterable: ' + str(input_iterable))
    print('-> Content current iterable: ' + str(current_iterable))
    print('Actual Result: Content merged iterable is ' + str(result))
    print('Expected Result: Content merged iterable should be ' + str(expected_result))
    assert result == expected_result
