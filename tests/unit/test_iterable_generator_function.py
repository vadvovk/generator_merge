from src.generator_merge import iterable_generator
import pytest


@pytest.mark.parametrize('generated_iterable_len_start, generated_iterable_len_end,'
                         'iterable_value_min, iterable_value_max, expected_result',
                         [(1, 5, 5, 20, 5),
                          (10, 20, 0, 10, 20),
                          ])
def test_iterable_len_after_generating(generated_iterable_len_start, generated_iterable_len_end,
                                       iterable_value_min, iterable_value_max, expected_result):
    print('\nTest Description: Check that random length of generated iterable '
          'less then specified random length in generated_iterable_len_end variable')
    result = iterable_generator(generated_iterable_len_start, generated_iterable_len_end,
                                iterable_value_min, iterable_value_max)
    print('The data for evidence:')
    print('-> Generated iterable: ' + str(result))
    result = len(result)
    print('Actual Result: Length generated iterable is ' + str(result))
    print('Expected Result: Length generated iterable should be less then ' + str(expected_result))
    assert result < expected_result


@pytest.mark.parametrize('generated_iterable_len_start, generated_iterable_len_end,'
                         'iterable_value_min, iterable_value_max, expected_result',
                         [(1, 5, 99, 1000, 1000),
                          (10, 20, 0, 10, 10),
                          (1, 20, 0, 2, 2),
                          ])
def test_iterable_max_value_after_generating(generated_iterable_len_start, generated_iterable_len_end,
                                             iterable_value_min, iterable_value_max, expected_result):
    print('\nTest Description: Check that random maximum generated value in generated iterable '
          'less then specified random maximum value in iterable_value_max variable')
    result = iterable_generator(generated_iterable_len_start, generated_iterable_len_end,
                                iterable_value_min, iterable_value_max)
    print('The data for evidence:')
    print('-> Generated iterable: ' + str(result))
    result = max(result)
    print('Actual Result: Maximum random value in generated iterable is ' + str(result))
    print('Expected Result: Maximum random value in generated iterable should be less then ' + str(expected_result))
    assert result < expected_result
