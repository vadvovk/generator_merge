def comma_separator(data):
    comma_separated = [str(element) for element in data]
    joined_string = ", ".join(comma_separated)

    return joined_string
